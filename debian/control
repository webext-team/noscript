Source: mozilla-noscript
Section: web
Priority: optional
Maintainer: Debian Mozilla Extension Maintainers <pkg-mozext-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           Jérémy Bobbio <lunar@debian.org>,
           David Prévot <taffit@debian.org>
Build-Depends: debhelper (>= 10), mozilla-devscripts
Standards-Version: 4.2.1
Homepage: http://noscript.net/
Vcs-Browser: https://salsa.debian.org/webext-team/noscript
Vcs-Git: https://salsa.debian.org/webext-team/noscript.git

Package: webext-noscript
Architecture: all
Depends: ${misc:Depends}, ${webext:Depends}
Recommends: ${webext:Recommends}
Provides: ${webext:Provides}
Enhances: ${webext:Enhances}
Replaces: ${webext:Replaces}, xul-ext-noscript (<< 10~~)
Breaks: ${webext:Breaks}, xul-ext-noscript (<< 10~~)
Conflicts: ${webext:Conflicts}
Description: permissions manager for Firefox
 This extension brings a powerful control over the way external scripts or
 embedded programs, such as Java or Flash, are loaded.
 .
 By blocking scripts and/or plugins, it improves security and disables annoying
 behaviours caused by malicious scripts.
 .
 When a script is blocked, you are notified, and you can unblock a site or a
 page, either temporarily or permanently. You can then set a whitelist of
 trusted sites based on URL or on domain name.

# https://wiki.debian.org/RenamingPackages
Package: xul-ext-noscript
Depends: webext-noscript, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: Show browser tabs like a tree - transitional package
 This is a transitional package. It can safely be removed.

